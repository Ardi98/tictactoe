# TicTacToe

TicTacToe Project

## TODOS:

- [ ] Haupt-Menü
    - [X] Hauptmenü-Hintergrund
    - [X] Hauptmenü-Buttons
        - Spielmenü
        - Hilfe
        - Einstellungen
        - Schließen
    - [X] Hauptmenü-Funktionalität
    - [ ] Ein '?' um Infos des Entwickler (und Quellen) anzugeben
        - [X] '?' Animation erstellt.
        - [X] About Screen erstellt.
- [X] Start-Menü
- [ ] Game
    - [X] Spielbar!
    - [ ] Neues Spiel (Button)

...

| Hauptmenü    | Startmenü     |
| :---         | :---          |
| Spielmenü    | Name: Spieler1|
| Hilfe        | Name: Spieler2|
| Einstellungen| Spiel Starten |
| Beenden      |               |

**Über den Entwickler**
<br>Name: Ardi Pelaj
<br>E-Mail: ardi98@live.de
<br>Beruf: Student
