package de.xenerus.ttt.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.xenerus.ttt.controller.TTTGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1280;
		config.height = 720;
		config.resizable = true;
		config.addIcon("ttt_files/TTT_ICON_128.png", Files.FileType.Internal);
		config.addIcon("ttt_files/TTT_ICON_32.png", Files.FileType.Internal);

		new LwjglApplication(new TTTGame(), config);
	}
}
