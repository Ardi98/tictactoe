package de.xenerus.ttt.data;

public enum MusicEnum {
    SOUND_BUTTON,
    SOUND_PLAY,
    GAME_MUSIC,
    TILE_SOUND,
    ALERT_SOUND
}
