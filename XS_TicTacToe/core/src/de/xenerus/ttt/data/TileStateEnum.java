package de.xenerus.ttt.data;

public enum TileStateEnum {
    CROSS,
    CIRCLE,
    FREE,
    DISABLED
}
