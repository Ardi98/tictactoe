package de.xenerus.ttt.data;

public enum FieldEnum {
    FIELD_CROSS,
    FIELD_CIRCLE,
    FIELD_RAW;
}
