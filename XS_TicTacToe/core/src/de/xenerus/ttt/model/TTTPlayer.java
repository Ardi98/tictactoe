package de.xenerus.ttt.model;

import de.xenerus.ttt.data.TileColorEnum;

import java.util.UUID;

public class TTTPlayer {

    private String name;
    private int wins;
    private UUID playerId;
    private TileColorEnum color;
    private boolean isColorRed;

    public TTTPlayer(String name, boolean isColorRed) {
        this.name = name;
        this.playerId = UUID.randomUUID();
        this.wins = 0;
        this.isColorRed = isColorRed;
        if(isColorRed) {
            color = TileColorEnum.ROT;
        } else {
            color = TileColorEnum.BLAU;
        }

    }

    public String getName() {
        return this.name;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getWins() {
        return this.wins;
    }

    public UUID getPlayerID() {
        return this.playerId;
    }

    public TileColorEnum getColor() {
        return this.color;
    }

    public void setColor(TileColorEnum color) {
        this.color = color;
    }

}
