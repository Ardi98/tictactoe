package de.xenerus.ttt.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TTTAnimator extends Actor {
    Animation animation;
    TextureRegion currentRegion;

    float time = 0f;

    public TTTAnimator(Animation animation) {
        this.animation = animation;
    }

    @Override
    public void act(float delta){
        super.act(delta);
        time += delta;

        currentRegion = (TextureRegion) animation.getKeyFrame(time, true);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(currentRegion, getX(), getY(), currentRegion.getRegionWidth()*0.5f, currentRegion.getRegionHeight()*0.5f);
    }
}
