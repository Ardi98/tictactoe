package de.xenerus.ttt.controller;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import de.xenerus.ttt.manager.AssetManager;
import de.xenerus.ttt.manager.MusicManager;
import de.xenerus.ttt.view.MainMenuScreen;

public class TTTGame extends Game {

    public static float HEIGHT = 720f;
    public static float WIDTH = 1280f;

    public static final String VERSION = "Alpha v1.0";

    public static SpriteBatch spriteBatch;
    public static Skin skin;
    public static MusicManager musicManager;

    private MainMenuScreen mainMenuScreen;

    @Override
    public void create() {
        Gdx.graphics.setTitle("TicTacToe - By XenSystems");

        skin = new Skin(Gdx.files.internal("glassy/skin/glassy-ui.json"));
        spriteBatch = new SpriteBatch();
        musicManager = new MusicManager();
        AssetManager.load();

        mainMenuScreen = new MainMenuScreen(this);
        showMainMenuScreen();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        System.exit(0);
    }

    public void showMainMenuScreen() {
        this.setScreen(mainMenuScreen);
    }

    public static Skin getSkin() {
        return TTTGame.skin;
    }

    public static MusicManager getMusicManager() {
        return TTTGame.musicManager;
    }

}
