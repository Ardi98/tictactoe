package de.xenerus.ttt.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import de.xenerus.ttt.data.MusicEnum;

public class MusicManager {

    private final Music button_sound;
    private final Music game_music;
    private final Music tile_sound;
    private final Music alert_sound;
    private boolean mute = false;

    public MusicManager() {
        button_sound = Gdx.audio.newMusic(Gdx.files.internal("sounds/button_sound.mp3"));
        game_music = Gdx.audio.newMusic(Gdx.files.internal("sounds/game_music.mp3"));
        tile_sound = Gdx.audio.newMusic(Gdx.files.internal("sounds/tile_click_sound.mp3"));
        alert_sound = Gdx.audio.newMusic(Gdx.files.internal("sounds/alert_sound.mp3"));
    }

    public void playSound(MusicEnum music) {
        if(music == MusicEnum.SOUND_BUTTON) {
            if(!mute) {
                button_sound.setVolume(1f);
            } else {
                button_sound.setVolume(0f);
            }
            button_sound.play();
        } else if(music == MusicEnum.GAME_MUSIC) {
            if(!game_music.isPlaying()) {
                game_music.play();
                game_music.setLooping(true);
            }
        } else if(music == MusicEnum.TILE_SOUND) {
            if(!mute) {
                tile_sound.setVolume(1f);
            } else {
                tile_sound.setVolume(0f);
            }
            tile_sound.play();
        } else if(music == MusicEnum.ALERT_SOUND) {
            if(!mute) {
                alert_sound.setVolume(1f);
            } else {
                alert_sound.setVolume(0f);
            }
            alert_sound.play();
        }
    }

    public Music getGameMusic() {
        return game_music;
    }

    public void setMuted(boolean muted) {
        mute = muted;
    }

    public boolean isMuted() {
        return mute;
    }

}
