package de.xenerus.ttt.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Pixmap;

public class AssetManager {

    public static void load() {
        setCursor();
        Gdx.app.log("ASSETLOADER", "geladen!");
    }

    public static void setCursor() {
        Pixmap pixmap = new Pixmap(Gdx.files.internal("ttt_files/TTT_CURSOR_32x32.png"));
        Cursor cursor = Gdx.graphics.newCursor(pixmap, 0,0);
        Gdx.graphics.setCursor(cursor);
        pixmap.dispose();
    }

    public static String getRawTilePath() {
        return "ttt_files/TTT_RAW.png";
    }

    public static String getCircleBluePath() {
        return "ttt_files/TTT_CIRCLE_BLUE.png";
    }

    public static String getCrossBluePath() {
        return "ttt_files/TTT_CROSS_BLUE.png";
    }

    public static String getCircleRedPath() {
        return "ttt_files/TTT_CIRCLE_RED.png";
    }

    public static String getCrossRedPath() {
        return "ttt_files/TTT_CROSS_RED.png";
    }

    public static String getCrossWinPath() {
        return "ttt_files/TTT_CROSS_WIN.png";
    }

    public static String getCircleWinPath() {
        return "ttt_files/TTT_CIRCLE_WIN.png";
    }


}
