package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;
import de.xenerus.ttt.data.TileColorEnum;
import de.xenerus.ttt.data.TileStateEnum;
import de.xenerus.ttt.manager.AssetManager;
import de.xenerus.ttt.model.TTTAnimator;
import de.xenerus.ttt.model.TTTPlayer;

import java.util.HashMap;

public class GameScreen implements Screen {

    private TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private Texture background;

    private Texture close;
    private ImageButton close_button;

    private Image field;

    private HashMap<Image, TileStateEnum> tile_map;
    private Image tile_1, tile_2, tile_3, tile_4, tile_5, tile_6, tile_7, tile_8, tile_9;

    private TTTPlayer playerOne;
    private TTTPlayer playerTwo;

    private Label playerOneName;
    private Label playerTwoName;

    private BitmapFont font;

    private boolean isPlayerOneTurn;

    private Window winInfo;

    private TextButton back_button;
    private TextButton replay_button;
    private Table buttonTable;

    private TextureAtlas gear_atlas;
    private Array<TextureAtlas.AtlasRegion> gearFrames;
    private static float GEAR_FRAME_DUR = 0.125f;
    private Animation<?> gearAnimation;
    private TTTAnimator gearAnimator;

    private Actor actor;

    public GameScreen(TTTGame game, TTTPlayer playerOne, TTTPlayer playerTwo) {
        this.game = game;
        skin = TTTGame.getSkin();
        tile_map = new HashMap<Image, TileStateEnum>();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        close = new Texture("ttt_files/TTT_CLOSE_BUTTON.png");
        Drawable close_drawable = new TextureRegionDrawable(new TextureRegion(close));
        close_button = new ImageButton(close_drawable);
        close_button.setSize(50, 50);
        close_button.setPosition(TTTGame.WIDTH/2 - close_button.getWidth()/2 + 590, TTTGame.HEIGHT/2 + 287.5f);

        close_button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new MainMenuScreen(game));
            }
        });

        gear_atlas = new TextureAtlas(Gdx.files.internal("animation/gear/zahnrad.atlas"));
        gearFrames = gear_atlas.findRegions("drehen");
        gearAnimation = new Animation<>(GEAR_FRAME_DUR, gearFrames, Animation.PlayMode.LOOP);
        gearAnimator = new TTTAnimator(gearAnimation);
        gearAnimator.setPosition(TTTGame.WIDTH/2 + 480, TTTGame.HEIGHT/2 + 280);

        actor = new Actor();
        actor.setSize(60, 62);
        actor.setPosition(TTTGame.WIDTH/2 + 480, TTTGame.HEIGHT/2 + 280);
        actor.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new SettingsScreen(game, GameScreen.this));
            }
        });

        buttonTable = new Table();

        back_button = new TextButton("Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        replay_button = new TextButton("Nochmal", skin, "small");
        createButton(replay_button, Color.RED, Color.ORANGE, 200);
        replay_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new GameScreen(game, playerOne, playerTwo));
                return true;
            }
        });

        buttonTable.add(replay_button).width(150).height(50).padRight(5).padTop(5);
        buttonTable.add(back_button).width(150).height(50).padLeft(5).padTop(5);

        field = new Image(new Texture("ttt_files/TTT_RAW_FIELD.png"));
        field.setSize(690, 690);
        field.setPosition(TTTGame.WIDTH/2 - field.getWidth()/2, TTTGame.HEIGHT/2 - field.getHeight()/2);

        this.playerOne = playerOne;
        this.playerTwo = playerTwo;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sanford.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.minFilter = Texture.TextureFilter.Nearest;
        parameter.genMipMaps = true;
        parameter.size = 20;
        font = generator.generateFont(parameter);

        int zufall = (int) Math.round(Math.random()*1);
        if(zufall == 0) {
            isPlayerOneTurn = true;
            playerOneName = new Label("Spieler 1: "+playerOne.getName()+" (Turn)", new Label.LabelStyle(font, Color.RED));
            playerTwoName = new Label("Spieler 2: "+playerTwo.getName(), new Label.LabelStyle(font, Color.RED));
        } else {
            isPlayerOneTurn = false;
            playerOneName = new Label("Spieler 1: "+playerOne.getName(), new Label.LabelStyle(font, Color.RED));
            playerTwoName = new Label("Spieler 2: "+playerTwo.getName()+" (Turn)", new Label.LabelStyle(font, Color.RED));
        }

        setPlayerNamePosition(playerOneName, 350, 200);
        setPlayerNamePosition(playerTwoName, 350, 150);

        winInfo = new Window("Gewonnen", skin);
        Label winLabel = new Label("", new Label.LabelStyle(font, Color.RED));
        winInfo.setColor(Color.ORANGE);
        winInfo.setSize(300, 200);
        winInfo.setPosition(TTTGame.WIDTH/2 - winInfo.getWidth()/2, TTTGame.HEIGHT/2 - winInfo.getHeight()/2);
        winInfo.setVisible(false);

        initializeTiles();

        setTileOnField(tile_1, - 230f, 230f);
        setTileOnField(tile_2, 0f, 230f);
        setTileOnField(tile_3, 230f, 230f);
        setTileOnField(tile_4, - 230f, 0f);
        setTileOnField(tile_5, 0f, 0f);
        setTileOnField(tile_6, 230f, 0f);
        setTileOnField(tile_7, - 230f, - 230f);
        setTileOnField(tile_8, 0f, - 230f);
        setTileOnField(tile_9, 230f, - 230f);

        setState(tile_1, TileStateEnum.FREE);
        setState(tile_2, TileStateEnum.FREE);
        setState(tile_3, TileStateEnum.FREE);
        setState(tile_4, TileStateEnum.FREE);
        setState(tile_5, TileStateEnum.FREE);
        setState(tile_6, TileStateEnum.FREE);
        setState(tile_7, TileStateEnum.FREE);
        setState(tile_8, TileStateEnum.FREE);
        setState(tile_9, TileStateEnum.FREE);

        tile_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_1) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_1 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_1 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_1, -230f, 230f);
                        setState(tile_1, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_1 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_1 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_1, -230f, 230f);
                        setState(tile_1, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_1, true);
                    stage.addActor(tile_1);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_2.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_2) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_2 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_2 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_2, 0f, 230f);
                        setState(tile_2, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_2 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_2 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_2, 0f, 230f);
                        setState(tile_2, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_2, true);
                    stage.addActor(tile_2);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_3.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_3) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_3 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_3 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_3, 230f, 230f);
                        setState(tile_3, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_3 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_3 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_3, 230f, 230f);
                        setState(tile_3, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_3, true);
                    stage.addActor(tile_3);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_4.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_4) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_4 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_4 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_4, -230f, 0f);
                        setState(tile_4, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_4 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_4 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_4, -230f, 0f);
                        setState(tile_4, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_4, true);
                    stage.addActor(tile_4);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_5.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_5) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_5 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_5 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_5, 0f, 0f);
                        setState(tile_5, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_5 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_5 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_5, 0f, 0f);
                        setState(tile_5, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_5, true);
                    stage.addActor(tile_5);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_6.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_6) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_6 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_6 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_6, 230f, 0f);
                        setState(tile_6, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_6 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_6 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_6, 230f, 0f);
                        setState(tile_6, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_6, true);
                    stage.addActor(tile_6);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_7.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_7) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_7 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_7 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_7, -230f, -230f);
                        setState(tile_7, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_7 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_7 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_7, -230f, -230f);
                        setState(tile_7, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_7, true);
                    stage.addActor(tile_7);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_8.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_8) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_8 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_8 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_8, 0f, -230f);
                        setState(tile_8, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_8 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_8 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_8, 0f, -230f);
                        setState(tile_8, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_8, true);
                    stage.addActor(tile_8);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });

        tile_9.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.TILE_SOUND);
                if (getState(tile_9) == TileStateEnum.FREE) {
                    if (isPlayerOneTurn) {
                        if (playerOne.getColor() == TileColorEnum.ROT) {
                            tile_9 = new Image(new Texture(AssetManager.getCircleRedPath()));
                        } else {
                            tile_9 = new Image(new Texture(AssetManager.getCircleBluePath()));
                        }
                        setTileOnField(tile_9, 230f, -230f);
                        setState(tile_9, TileStateEnum.CIRCLE);
                    } else {
                        if (playerTwo.getColor() == TileColorEnum.ROT) {
                            tile_9 = new Image(new Texture(AssetManager.getCrossRedPath()));
                        } else {
                            tile_9 = new Image(new Texture(AssetManager.getCrossBluePath()));
                        }
                        setTileOnField(tile_9, 230f, -230f);
                        setState(tile_9, TileStateEnum.CROSS);
                    }
                    stage.getActors().removeValue(tile_9, true);
                    stage.addActor(tile_9);
                    if (checkWin()) {
                        setState(tile_1, TileStateEnum.DISABLED);
                        setState(tile_2, TileStateEnum.DISABLED);
                        setState(tile_3, TileStateEnum.DISABLED);
                        setState(tile_4, TileStateEnum.DISABLED);
                        setState(tile_5, TileStateEnum.DISABLED);
                        setState(tile_6, TileStateEnum.DISABLED);
                        setState(tile_7, TileStateEnum.DISABLED);
                        setState(tile_8, TileStateEnum.DISABLED);
                        setState(tile_9, TileStateEnum.DISABLED);
                        if (isPlayerOneTurn) {
                            winLabel.setText("Spieler 1 " + playerOne.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        } else {
                            winLabel.setText("Spieler 2 " + playerTwo.getName() + " hat gewonnen!");
                            winInfo.add(winLabel).row();
                            winInfo.add(buttonTable).align(Align.bottom).row();
                            winInfo.pack();
                            stage.addActor(winInfo);
                            winInfo.setVisible(true);
                        }
                    } else if(checkDraw()) {
                        game.setScreen(new GameScreen(game, playerOne, playerTwo));
                    } else {
                        changePlayer();
                    }
                }
            }
        });
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(close_button);
        stage.addActor(gearAnimator);
        stage.addActor(actor);
        stage.addActor(field);
        stage.addActor(tile_1);
        stage.addActor(tile_2);
        stage.addActor(tile_3);
        stage.addActor(tile_4);
        stage.addActor(tile_5);
        stage.addActor(tile_6);
        stage.addActor(tile_7);
        stage.addActor(tile_8);
        stage.addActor(tile_9);
        stage.addActor(playerOneName);
        stage.addActor(playerTwoName);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
    }

    public void initializeTiles() {
        tile_1 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_2 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_3 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_4 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_5 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_6 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_7 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_8 = new Image(new Texture("ttt_files/TTT_RAW.png"));
        tile_9 = new Image(new Texture("ttt_files/TTT_RAW.png"));
    }

    public void setTileOnField(Image tile, float offsetX, float offsetY) {
        tile.setSize(221.5f, 221.5f);
        tile.setPosition(TTTGame.WIDTH/2 - tile_1.getWidth()/2 + offsetX, TTTGame.HEIGHT/2 - tile_1.getHeight()/2 + offsetY);
    }

    public void setPlayerNamePosition(Label name, float offsetX, float offsetY) {
        name.setPosition(TTTGame.WIDTH/2 + offsetX, TTTGame.HEIGHT/2 - name.getHeight()/2 + offsetY);
    }

    public TileStateEnum getState(Image tile) {
        if(tile_map.isEmpty()) {
            return null;
        } else {
            if(tile_map.containsKey(tile)) {
                return tile_map.get(tile);
            }
        }
        return null;
    }

    public void setState(Image tile, TileStateEnum state) {
        if(!tile_map.containsKey(tile)) {
            tile_map.put(tile, state);
        } else {
            if(!(tile_map.get(tile) == state)) {
                tile_map.replace(tile, state);
            }
        }
    }

    public void changePlayer() {
        if(isPlayerOneTurn) {
            isPlayerOneTurn = false;
            playerTwoName.setText("Spieler 2: "+playerTwo.getName() + " (Turn)");
            playerOneName.setText("Spieler 1: "+playerOne.getName());
        } else {
            isPlayerOneTurn = true;
            playerTwoName.setText("Spieler 2: "+playerTwo.getName());
            playerOneName.setText("Spieler 1: "+playerOne.getName() + " (Turn)");
        }
    }

    public boolean checkWin() {
        if(getState(tile_1) == TileStateEnum.CIRCLE && getState(tile_2) == TileStateEnum.CIRCLE && getState(tile_3) == TileStateEnum.CIRCLE) {
            tile_1 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_2 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_2, 0f, 230f);
            stage.getActors().removeValue(tile_2, true);
            stage.addActor(tile_2);

            tile_3 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);
            return true;
        } else if(getState(tile_4) == TileStateEnum.CIRCLE && getState(tile_5) == TileStateEnum.CIRCLE && getState(tile_6) == TileStateEnum.CIRCLE) {
            tile_4 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_4, - 230f, 0f);
            stage.getActors().removeValue(tile_4, true);
            stage.addActor(tile_4);

            tile_5 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_6 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_6, 230f, 0f);
            stage.getActors().removeValue(tile_6, true);
            stage.addActor(tile_6);
            return true;
        } else if(getState(tile_7) == TileStateEnum.CIRCLE && getState(tile_8) == TileStateEnum.CIRCLE && getState(tile_9) == TileStateEnum.CIRCLE) {
            tile_7 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);

            tile_8 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_8, 0f, - 230f);
            stage.getActors().removeValue(tile_8, true);
            stage.addActor(tile_8);

            tile_9 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_1) == TileStateEnum.CIRCLE && getState(tile_4) == TileStateEnum.CIRCLE && getState(tile_7) == TileStateEnum.CIRCLE) {
            tile_1 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_4 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_4, - 230f, 0f);
            stage.getActors().removeValue(tile_4, true);
            stage.addActor(tile_4);

            tile_7 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);
            return true;
        } else if(getState(tile_2) == TileStateEnum.CIRCLE && getState(tile_5) == TileStateEnum.CIRCLE && getState(tile_8) == TileStateEnum.CIRCLE) {
            tile_2 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_2, 0f, 230f);
            stage.getActors().removeValue(tile_2, true);
            stage.addActor(tile_2);

            tile_5 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_8 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_8, 0f, - 230f);
            stage.getActors().removeValue(tile_8, true);
            stage.addActor(tile_8);
            return true;
        } else if(getState(tile_3) == TileStateEnum.CIRCLE && getState(tile_6) == TileStateEnum.CIRCLE && getState(tile_9) == TileStateEnum.CIRCLE) {
            tile_3 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);

            tile_6 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_6, 230f, 0f);
            stage.getActors().removeValue(tile_6, true);
            stage.addActor(tile_6);

            tile_9 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_1) == TileStateEnum.CIRCLE && getState(tile_5) == TileStateEnum.CIRCLE && getState(tile_9) == TileStateEnum.CIRCLE) {
            tile_1 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_5 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_9 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_3) == TileStateEnum.CIRCLE && getState(tile_5) == TileStateEnum.CIRCLE && getState(tile_7) == TileStateEnum.CIRCLE) {
            tile_3 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);

            tile_5 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_7 = new Image(new Texture(AssetManager.getCircleWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);
            return true;
        } else if(getState(tile_1) == TileStateEnum.CROSS && getState(tile_2) == TileStateEnum.CROSS && getState(tile_3) == TileStateEnum.CROSS) {
            tile_1 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_2 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_2, 0f, 230f);
            stage.getActors().removeValue(tile_2, true);
            stage.addActor(tile_2);

            tile_3 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);
            return true;
        } else if(getState(tile_4) == TileStateEnum.CROSS && getState(tile_5) == TileStateEnum.CROSS && getState(tile_6) == TileStateEnum.CROSS) {
            tile_4 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_4, - 230f, 0f);
            stage.getActors().removeValue(tile_4, true);
            stage.addActor(tile_4);

            tile_5 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_6 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_6, 230f, 0f);
            stage.getActors().removeValue(tile_6, true);
            stage.addActor(tile_6);
            return true;
        } else if(getState(tile_7) == TileStateEnum.CROSS && getState(tile_8) == TileStateEnum.CROSS && getState(tile_9) == TileStateEnum.CROSS) {
            tile_7 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);

            tile_8 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_8, 0f, - 230f);
            stage.getActors().removeValue(tile_8, true);
            stage.addActor(tile_8);

            tile_9 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_1) == TileStateEnum.CROSS && getState(tile_4) == TileStateEnum.CROSS && getState(tile_7) == TileStateEnum.CROSS) {
            tile_1 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_4 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_4, - 230f, 0f);
            stage.getActors().removeValue(tile_4, true);
            stage.addActor(tile_4);

            tile_7 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);
            return true;
        } else if(getState(tile_2) == TileStateEnum.CROSS && getState(tile_5) == TileStateEnum.CROSS && getState(tile_8) == TileStateEnum.CROSS) {
            tile_2 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_2, 0f, 230f);
            stage.getActors().removeValue(tile_2, true);
            stage.addActor(tile_2);

            tile_5 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_8 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_8, 0f, - 230f);
            stage.getActors().removeValue(tile_8, true);
            stage.addActor(tile_8);
            return true;
        } else if(getState(tile_3) == TileStateEnum.CROSS && getState(tile_6) == TileStateEnum.CROSS && getState(tile_9) == TileStateEnum.CROSS) {
            tile_3 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);

            tile_6 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_6, 230f, 0f);
            stage.getActors().removeValue(tile_6, true);
            stage.addActor(tile_6);

            tile_9 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_1) == TileStateEnum.CROSS && getState(tile_5) == TileStateEnum.CROSS && getState(tile_9) == TileStateEnum.CROSS) {
            tile_1 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_1, - 230f, 230f);
            stage.getActors().removeValue(tile_1, true);
            stage.addActor(tile_1);

            tile_5 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_9 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_9, 230f, - 230f);
            stage.getActors().removeValue(tile_9, true);
            stage.addActor(tile_9);
            return true;
        } else if(getState(tile_3) == TileStateEnum.CROSS && getState(tile_5) == TileStateEnum.CROSS && getState(tile_7) == TileStateEnum.CROSS) {
            tile_3 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_3, 230f, 230f);
            stage.getActors().removeValue(tile_3, true);
            stage.addActor(tile_3);

            tile_5 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_5, 0f, 0f);
            stage.getActors().removeValue(tile_5, true);
            stage.addActor(tile_5);

            tile_7 = new Image(new Texture(AssetManager.getCrossWinPath()));
            setTileOnField(tile_7, - 230f, - 230f);
            stage.getActors().removeValue(tile_7, true);
            stage.addActor(tile_7);
            return true;
        } else {
            return false;
        }
    }

    public boolean checkDraw() {
        if(getState(tile_1) != TileStateEnum.FREE && getState(tile_2) != TileStateEnum.FREE
            && getState(tile_3) != TileStateEnum.FREE && getState(tile_4) != TileStateEnum.FREE
            && getState(tile_5) != TileStateEnum.FREE && getState(tile_6) != TileStateEnum.FREE
            && getState(tile_7) != TileStateEnum.FREE && getState(tile_8) != TileStateEnum.FREE
            && getState(tile_9) != TileStateEnum.FREE) {
            return true;
        } else {
            return false;
        }
    }

}
