package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;
import de.xenerus.ttt.data.TileColorEnum;
import de.xenerus.ttt.model.TTTPlayer;

public class StartScreen implements Screen {

    private TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private Texture background;

    private TextButton online_button;
    private TextButton offline_button;
    private TextButton start_button;
    private TextButton back_button;
    private TextButton ok_button;

    private TextField playerOne;
    private TextField playerTwo;

    private Label playerOneName;
    private Label playerTwoName;

    private TTTPlayer tttPlayerOne;
    private TTTPlayer tttPlayerTwo;

    private Window alert;
    private boolean isOnlineVisible = false;

    public StartScreen(TTTGame game) {
        this.game = game;
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");

        BitmapFont font;
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sanford.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.minFilter = Texture.TextureFilter.Nearest;
        parameter.genMipMaps = true;
        parameter.size = 26;
        font = generator.generateFont(parameter);

        ok_button = new TextButton("Ok", skin, "small");
        ok_button.setTransform(true);
        ok_button.setWidth(200);
        ok_button.setColor(Color.RED);
        ok_button.getLabel().setColor(Color.ORANGE);
        ok_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                alert.setVisible(false);
                back_button.setVisible(true);
                if(!isOnlineVisible) {
                    offline_button.setVisible(true);
                } else {
                    online_button.setVisible(true);
                    isOnlineVisible = false;
                }
                start_button.setVisible(true);
                return true;
            }
        });

        back_button = new TextButton("Umkehren zum Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200, 0, -300);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        offline_button = new TextButton("Offline-Mode", skin, "small");
        createButton(offline_button, Color.RED, Color.ORANGE, 200, -105, -225);
        offline_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                playerOneName.setText("Spieler Name: ");
                offline_button.setVisible(false);
                online_button.setVisible(true);
                playerTwoName.setVisible(false);
                playerTwo.setVisible(false);
                return true;
            }
        });

        online_button = new TextButton("Online-Mode", skin, "small");
        createButton(online_button, Color.RED, Color.ORANGE, 200, -105, -225);
        online_button.setVisible(false);
        online_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                playerOneName.setText("Name Spieler 1: ");
                online_button.setVisible(false);
                offline_button.setVisible(true);
                playerTwoName.setVisible(true);
                playerTwo.setVisible(true);
                return true;
            }
        });

        start_button = new TextButton("Starte Spiel", skin, "small");
        createButton(start_button, Color.RED, Color.ORANGE, 200, 105, -225);
        start_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                if(offline_button.isVisible()) {
                    if (playerOne.getText().isEmpty() || playerTwo.getText().isEmpty()) {
                        //TODO: Window with Alert
                        TTTGame.getMusicManager().playSound(MusicEnum.ALERT_SOUND);
                        alert = new Window("Achtung", skin);
                        alert.getTitleLabel().setAlignment(Align.center);
                        Label alertLabel = new Label("", new Label.LabelStyle(font, Color.RED));
                        alert.setColor(Color.ORANGE);
                        alert.setSize(300, 200);
                        alert.setVisible(false);
                        alertLabel.setText("Beide Spieler müssen einen Namen haben!");
                        alert.add(alertLabel).row();
                        alert.add(ok_button).width(200).height(50).padTop(20).align(Align.bottom).row();
                        alert.pack();
                        alert.setPosition(TTTGame.WIDTH / 2 - alert.getWidth() / 2, TTTGame.HEIGHT / 2 - alert.getHeight() / 2);
                        stage.getActors().removeValue(alert, true);
                        stage.addActor(alert);
                        alert.setVisible(true);
                        back_button.setVisible(false);
                        offline_button.setVisible(false);
                        online_button.setVisible(false);
                        start_button.setVisible(false);
                    } else if (((playerOne.getText().length() < 3) || (playerOne.getText().length() > 8)) || ((playerTwo.getText().length() < 3) || (playerTwo.getText().length() > 8))) {
                        //TODO: Window with Alert
                        TTTGame.getMusicManager().playSound(MusicEnum.ALERT_SOUND);
                        alert = new Window("Achtung", skin);
                        alert.getTitleLabel().setAlignment(Align.center);
                        Label alertLabel = new Label("", new Label.LabelStyle(font, Color.RED));
                        alert.setColor(Color.ORANGE);
                        alert.setSize(300, 200);
                        alert.setVisible(false);
                        alertLabel.setText("Namen müssen mindestens 3 Zeichen enthalten\nund dürfen maximal 8 Zeichen lang sein!");
                        alertLabel.setAlignment(Align.center);
                        alert.add(alertLabel).row();
                        alert.add(ok_button).width(200).height(50).padTop(20).align(Align.bottom).row();
                        alert.pack();
                        alert.setPosition(TTTGame.WIDTH / 2 - alert.getWidth() / 2, TTTGame.HEIGHT / 2 - alert.getHeight() / 2);
                        stage.getActors().removeValue(alert, true);
                        stage.addActor(alert);
                        alert.setVisible(true);
                        back_button.setVisible(false);
                        offline_button.setVisible(false);
                        online_button.setVisible(false);
                        start_button.setVisible(false);
                    } else if (playerOne.getText().equalsIgnoreCase(playerTwo.getText())) {
                        //TODO: Window with Alert
                        TTTGame.getMusicManager().playSound(MusicEnum.ALERT_SOUND);
                        alert = new Window("Achtung", skin);
                        alert.getTitleLabel().setAlignment(Align.center);
                        Label alertLabel = new Label("", new Label.LabelStyle(font, Color.RED));
                        alert.setColor(Color.ORANGE);
                        alert.setSize(300, 200);
                        alert.setVisible(false);
                        alertLabel.setText("Spieler dürfen nicht den selben Namen haben!");
                        alert.add(alertLabel).row();
                        alert.add(ok_button).width(200).height(50).padTop(20).align(Align.bottom).row();
                        alert.pack();
                        alert.setPosition(TTTGame.WIDTH / 2 - alert.getWidth() / 2, TTTGame.HEIGHT / 2 - alert.getHeight() / 2);
                        stage.getActors().removeValue(alert, true);
                        stage.addActor(alert);
                        alert.setVisible(true);
                        back_button.setVisible(false);
                        offline_button.setVisible(false);
                        start_button.setVisible(false);
                    } else {
                        int zufall = (int) Math.round(Math.random() * 1);
                        boolean isColorRed;
                        if (zufall == 0) {
                            isColorRed = false;
                        } else {
                            isColorRed = true;
                        }
                        TTTPlayer player1 = new TTTPlayer(playerOne.getText(), isColorRed);
                        TTTPlayer player2;
                        if (player1.getColor() == TileColorEnum.ROT) {
                            player2 = new TTTPlayer(playerTwo.getText(), false);
                        } else {
                            player2 = new TTTPlayer(playerTwo.getText(), true);
                        }
                        game.setScreen(new GameScreen(game, player1, player2));
                    }
                } else {
                    TTTGame.getMusicManager().playSound(MusicEnum.ALERT_SOUND);
                    alert = new Window("Achtung", skin);
                    alert.getTitleLabel().setAlignment(Align.center);
                    Label alertLabel = new Label("", new Label.LabelStyle(font, Color.RED));
                    alert.setColor(Color.ORANGE);
                    alert.setSize(300, 200);
                    alert.setVisible(false);
                    alertLabel.setText("Der Online-Modus ist noch nicht verfügbar!");
                    alertLabel.setAlignment(Align.center);
                    alert.add(alertLabel).row();
                    alert.add(ok_button).width(200).height(50).padTop(20).align(Align.bottom).row();
                    alert.pack();
                    alert.setPosition(TTTGame.WIDTH / 2 - alert.getWidth() / 2, TTTGame.HEIGHT / 2 - alert.getHeight() / 2);
                    stage.getActors().removeValue(alert, true);
                    stage.addActor(alert);
                    alert.setVisible(true);
                    online_button.setVisible(false);
                    back_button.setVisible(false);
                    start_button.setVisible(false);
                    isOnlineVisible = true;
                }
                return true;
            }
        });

        playerOne = new TextField("", skin);
        playerOne.setPosition(TTTGame.WIDTH/2 - playerOne.getWidth()/2 + 100, TTTGame.HEIGHT/2 + 25);
        playerOne.setColor(new Color(Color.rgba8888(0.52f, 0.0f, 0.0f, 1f)));

        playerTwo = new TextField("", skin);
        playerTwo.setPosition(TTTGame.WIDTH/2 - playerTwo.getWidth()/2 + 100, TTTGame.HEIGHT/2 - 50);
        playerTwo.setColor(new Color(Color.rgba8888(0.52f, 0.0f, 0.0f, 1f)));

        playerOneName = new Label("Name Spieler 1:", new Label.LabelStyle(font, Color.RED));
        playerOneName.setPosition(TTTGame.WIDTH/2 - playerOneName.getWidth()/2 - 90, TTTGame.HEIGHT/2 + 30);
        playerTwoName = new Label("Name Spieler 2:", new Label.LabelStyle(font, Color.RED));
        playerTwoName.setPosition(TTTGame.WIDTH/2 - playerTwoName.getWidth()/2 - 90, TTTGame.HEIGHT/2 - 45);
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(playerOneName);
        stage.addActor(playerTwoName);
        stage.addActor(back_button);
        stage.addActor(start_button);
        stage.addActor(offline_button);
        stage.addActor(online_button);
        stage.addActor(playerOne);
        stage.addActor(playerTwo);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width, float widthmodifier, float heightmodifier) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
        button.setPosition(TTTGame.WIDTH/2-button.getWidth()/2+widthmodifier, TTTGame.HEIGHT/2+heightmodifier);
    }
}
