package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;

public class SettingsScreen implements Screen {

    private TTTGame game;
    private GameScreen gameScreen;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private TextButton back_button;

    private Texture background;

    private Slider slider;

    private Label volume;
    private Label size;

    private CheckBox checkBox;

    private Image settings_title;

    private SelectBox<String> sizeBox;

    public SettingsScreen(TTTGame game) {
        this.game = game;
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        settings_title = new Image(new Texture("ttt_files/TTT_SETTINGS_TITLE.png"));
        settings_title.setPosition(TTTGame.WIDTH/2 - settings_title.getWidth()/2, TTTGame.HEIGHT/2 + 200);

        back_button = new TextButton("Umkehren zum Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200, -300);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                dispose();
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        BitmapFont font;
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sanford.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.minFilter = Texture.TextureFilter.Nearest;
        parameter.genMipMaps = true;
        parameter.size = 26;
        font = generator.generateFont(parameter);

        slider = new Slider(0f, 1f, 0.01f, false, skin);
        slider.setWidth(300);
        slider.setPosition(TTTGame.WIDTH/2-slider.getWidth()/2, TTTGame.HEIGHT/2);
        slider.setColor(Color.RED);
        slider.setValue(TTTGame.getMusicManager().getGameMusic().getVolume());
        volume = new Label(String.format("Musik Lautstärke: %.0f%%",slider.getValue()*100), new Label.LabelStyle(font, Color.MAGENTA));
        volume.setPosition(TTTGame.WIDTH/2-volume.getWidth()/2, TTTGame.HEIGHT/2+50);
        volume.setColor(Color.RED);
        slider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TTTGame.getMusicManager().getGameMusic().setVolume(slider.getValue());
                volume.setText(String.format("Musik Lautstärke: %.0f%%",slider.getValue()*100));
            }
        });

        checkBox = new CheckBox(" Click-Sound Stummschalten", skin);
        checkBox.setPosition(TTTGame.WIDTH/2 - checkBox.getWidth()/2, TTTGame.HEIGHT/2-50);
        checkBox.getLabel().setColor(Color.RED);
        checkBox.getImage().setColor(Color.RED);
        if(TTTGame.getMusicManager().isMuted()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }
        checkBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(checkBox.isChecked()) {
                    TTTGame.getMusicManager().setMuted(true);
                } else {
                    TTTGame.getMusicManager().setMuted(false);
                }
            }
        });

        size = new Label("Fenstergröße einstellen:", new Label.LabelStyle(font, Color.RED));
        size.setPosition(TTTGame.WIDTH/2 - size.getWidth()/2 + 420, TTTGame.HEIGHT/2 + 50);

        sizeBox = new SelectBox<String>(skin);
        sizeBox.setWidth(300);
        sizeBox.setColor(Color.RED);
        sizeBox.getList().setColor(Color.RED);
        Array<String> selectList = new Array<String>();
        if(!Gdx.graphics.isFullscreen()) {
            if(Gdx.graphics.getWidth() == 1280) {
                selectList.add("1280x720 (empfohlen)");
                selectList.add("1440x900");
                selectList.add("1920x1080");
            } else if(Gdx.graphics.getWidth() == 1440) {
                selectList.add("1440x900");
                selectList.add("1280x720 (empfohlen)");
                selectList.add("1920x1080");
            }
        } else {
            selectList.add("1920x1080");
            selectList.add("1280x720 (empfohlen)");
            selectList.add("1440x900");
        }
        sizeBox.setItems(selectList);
        sizeBox.setPosition(TTTGame.WIDTH/2 - sizeBox.getWidth()/2 + 420, TTTGame.HEIGHT/2);
        sizeBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(sizeBox.getSelected().equalsIgnoreCase("1280x720 (empfohlen)")) {
                    Gdx.graphics.setWindowedMode(1280, 720);
                    game.resize(1280, 720);
                    sizeBox.getList().getItems().set(0, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(1, "1440x900");
                    sizeBox.getList().getItems().set(2, "1920x1080");
                } else if(sizeBox.getSelected().equalsIgnoreCase("1920x1080")) {
                    Gdx.graphics.setWindowedMode(1920, 1080);
                    Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                    game.resize(1920, 1080);
                    sizeBox.getList().getItems().set(1, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(2, "1440x900");
                    sizeBox.getList().getItems().set(0, "1920x1080");
                } else if(sizeBox.getSelected().equalsIgnoreCase("1440x900")) {
                    Gdx.graphics.setWindowedMode(1440, 900);
                    game.resize(1440, 900);
                    sizeBox.getList().getItems().set(1, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(0, "1440x900");
                    sizeBox.getList().getItems().set(2, "1920x1080");
                }
            }
        });
    }

    public SettingsScreen(TTTGame game, GameScreen gameScreen) {
        this.game = game;
        this.gameScreen = gameScreen;
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        settings_title = new Image(new Texture("ttt_files/TTT_SETTINGS_TITLE.png"));
        settings_title.setPosition(TTTGame.WIDTH/2 - settings_title.getWidth()/2, TTTGame.HEIGHT/2 + 200);

        back_button = new TextButton("Umkehren zum Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200, -300);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                dispose();
                game.setScreen(gameScreen);
                return true;
            }
        });

        BitmapFont font;
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sanford.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.minFilter = Texture.TextureFilter.Nearest;
        parameter.genMipMaps = true;
        parameter.size = 26;
        font = generator.generateFont(parameter);

        slider = new Slider(0f, 1f, 0.01f, false, skin);
        slider.setWidth(300);
        slider.setPosition(TTTGame.WIDTH/2-slider.getWidth()/2, TTTGame.HEIGHT/2);
        slider.setColor(Color.RED);
        slider.setValue(TTTGame.getMusicManager().getGameMusic().getVolume());
        volume = new Label(String.format("Musik Lautstärke: %.0f%%",slider.getValue()*100), new Label.LabelStyle(font, Color.MAGENTA));
        volume.setPosition(TTTGame.WIDTH/2-volume.getWidth()/2, TTTGame.HEIGHT/2+50);
        volume.setColor(Color.RED);
        slider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TTTGame.getMusicManager().getGameMusic().setVolume(slider.getValue());
                volume.setText(String.format("Musik Lautstärke: %.0f%%",slider.getValue()*100));
            }
        });

        checkBox = new CheckBox(" Click-Sound Stummschalten", skin);
        checkBox.setPosition(TTTGame.WIDTH/2 - checkBox.getWidth()/2, TTTGame.HEIGHT/2-50);
        checkBox.getLabel().setColor(Color.RED);
        checkBox.getImage().setColor(Color.RED);
        if(TTTGame.getMusicManager().isMuted()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }
        checkBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(checkBox.isChecked()) {
                    TTTGame.getMusicManager().setMuted(true);
                } else {
                    TTTGame.getMusicManager().setMuted(false);
                }
            }
        });

        size = new Label("Fenstergröße einstellen:", new Label.LabelStyle(font, Color.RED));
        size.setPosition(TTTGame.WIDTH/2 - size.getWidth()/2 + 420, TTTGame.HEIGHT/2 + 50);

        sizeBox = new SelectBox<String>(skin);
        sizeBox.setWidth(300);
        sizeBox.setColor(Color.RED);
        sizeBox.getList().setColor(Color.RED);
        Array<String> selectList = new Array<String>();
        if(!Gdx.graphics.isFullscreen()) {
            if(Gdx.graphics.getWidth() == 1280) {
                selectList.add("1280x720 (empfohlen)");
                selectList.add("1440x900");
                selectList.add("1920x1080");
            } else if(Gdx.graphics.getWidth() == 1440) {
                selectList.add("1440x900");
                selectList.add("1280x720");
                selectList.add("1920x1080");
            }
        } else {
            selectList.add("1920x1080");
            selectList.add("1280x720 (empfohlen)");
            selectList.add("1440x900");
        }
        sizeBox.setItems(selectList);
        sizeBox.setPosition(TTTGame.WIDTH/2 - sizeBox.getWidth()/2 + 420, TTTGame.HEIGHT/2);
        sizeBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(sizeBox.getSelected().equalsIgnoreCase("1280x720 (empfohlen)")) {
                    Gdx.graphics.setWindowedMode(1280, 720);
                    game.resize(1280, 720);
                    sizeBox.getList().getItems().set(0, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(1, "1440x900");
                    sizeBox.getList().getItems().set(2, "1920x1080");
                } else if(sizeBox.getSelected().equalsIgnoreCase("1920x1080")) {
                    Gdx.graphics.setWindowedMode(1920, 1080);
                    Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                    game.resize(1920, 1080);
                    sizeBox.getList().getItems().set(1, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(2, "1440x900");
                    sizeBox.getList().getItems().set(0, "1920x1080");
                } else if(sizeBox.getSelected().equalsIgnoreCase("1440x900")) {
                    Gdx.graphics.setWindowedMode(1440, 900);
                    game.resize(1440, 900);
                    sizeBox.getList().getItems().set(1, "1280x720 (empfohlen)");
                    sizeBox.getList().getItems().set(0, "1440x900");
                    sizeBox.getList().getItems().set(2, "1920x1080");
                }
            }
        });
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(settings_title);
        stage.addActor(back_button);
        stage.addActor(volume);
        stage.addActor(size);
        stage.addActor(slider);
        stage.addActor(checkBox);
        stage.addActor(sizeBox);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width, float heightmodifier) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
        button.setPosition(TTTGame.WIDTH/2-button.getWidth()/2, TTTGame.HEIGHT/2+heightmodifier);
    }
}
