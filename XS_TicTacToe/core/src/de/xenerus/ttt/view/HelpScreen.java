package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;
import de.xenerus.ttt.manager.AssetManager;

public class HelpScreen implements Screen {

    public TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private TextButton back_button;

    private ScrollPane scrollPane;
    private Table scrollBox;
    private Table scrollTable;

    private Texture background;
    private Image scroll_background;
    private Image circle;
    private Image cross;
    private Image circle_red;
    private Image cross_red;
    private Image win_circle;
    private Image win_cross;

    public HelpScreen(TTTGame game) {
        this.game = game;
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        scroll_background = new Image(new Texture("ttt_files/TTT_SCROLL_BACKGROUND.png"));
        scroll_background.setPosition(TTTGame.WIDTH/2 - scroll_background.getWidth()/2, TTTGame.HEIGHT/2 - scroll_background.getHeight()/2 + 75);
        circle = new Image(new Texture("ttt_files/TTT_CIRCLE_BLUE.png"));
        cross = new Image(new Texture("ttt_files/TTT_CROSS_BLUE.png"));
        win_circle = new Image(new Texture(AssetManager.getCircleWinPath()));
        win_cross = new Image(new Texture(AssetManager.getCrossWinPath()));
        circle_red = new Image(new Texture(AssetManager.getCircleRedPath()));
        cross_red = new Image(new Texture(AssetManager.getCrossRedPath()));

        back_button = new TextButton("Umkehren zum Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200, -300);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                dispose();
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        scrollBox = new Table();
        scrollBox.setFillParent(true);

        scrollTable = new Table();

        scrollPane = new ScrollPane(scrollTable, skin);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setColor(Color.RED);

        scrollTable.pad(10).defaults().expandX().space(4);

        Label begin = new Label("[Anleitung des Spiels]", skin);
        begin.setAlignment(Align.center);
        begin.setWrap(true);
        begin.setColor(Color.BLACK);
        begin.setFontScale(1.4f);
        scrollTable.add(begin).width(300);
        scrollTable.row();

        Label anleitung_start = new Label("1. Start", skin);
        anleitung_start.setAlignment(Align.center);
        anleitung_start.setWrap(true);
        anleitung_start.setColor(Color.BLACK);
        anleitung_start.setFontScale(1.2f);
        scrollTable.add(anleitung_start).width(300);
        scrollTable.row();

        Label start = new Label("Um das Spiel zu starten, muss auf den Button \"Starte Spiel\" geklickt werden. "+
                                     "Anschliessend waehlen Spieler1 und Spieler2 ihre Namen und klicken auf \"Los Geht's\". "+
                                     "Nun wirst du ein neues Bild sehen und zwar den Spielebildschirm. Hier beginnt das Spiel!", skin);
        start.setAlignment(Align.left);
        start.setWrap(true);
        start.setColor(Color.ORANGE);
        scrollTable.add(start).width(340);
        scrollTable.row();

        Label circle_title = new Label("2. Der 'Kreis'", skin);
        circle_title.setAlignment(Align.center);
        circle_title.setWrap(true);
        circle_title.setColor(Color.BLACK);
        circle_title.setFontScale(1.2f);
        scrollTable.add(circle_title).width(300);
        scrollTable.row();

        Table circletable = new Table();
        circletable.add(circle).width(100).height(100).padRight(10);
        circletable.add(circle_red).width(100).height(100).padLeft(10);
        scrollTable.add(circletable).align(Align.center);
        scrollTable.row();

        Label circle_description = new Label("Der Kreis ist ein wichtiger Bestandteil des Spiels und identifiziert einen Spieler. "+
                                                "Der Kreis kann auf ein freies Feld gesetzt werden, wenn der Spieler, "+
                                                "der \"Kreis\" zugeordnet bekommen hat, an der Reihe ist.", skin);
        circle_description.setAlignment(Align.left);
        circle_description.setWrap(true);
        circle_description.setColor(Color.ORANGE);
        scrollTable.add(circle_description).width(340);
        scrollTable.row();

        Label cross_title = new Label("3. Das 'X'", skin);
        cross_title.setAlignment(Align.center);
        cross_title.setWrap(true);
        cross_title.setColor(Color.BLACK);
        cross_title.setFontScale(1.2f);
        scrollTable.add(cross_title).width(300);
        scrollTable.row();

        Table crosstable = new Table();
        crosstable.add(cross).width(100).height(100).padRight(10);
        crosstable.add(cross_red).width(100).height(100).padLeft(10);
        scrollTable.add(crosstable).align(Align.center);
        scrollTable.row();

        Label cross_description = new Label("Das X ist wie auch der Kreis ein grundlegender Baustein des Spiels und identifiziert einen Spieler. "+
                                                "Das X kann auf ein freies Feld gesetzt werden, wenn der Spieler, "+
                                                "der \"X\" zugeordnet bekommen hat, an der Reihe ist.", skin);
        cross_description.setAlignment(Align.left);
        cross_description.setWrap(true);
        cross_description.setColor(Color.ORANGE);
        scrollTable.add(cross_description).width(340);
        scrollTable.row();

        Label winner = new Label("4. Der Gewinner", skin);
        winner.setAlignment(Align.center);
        winner.setWrap(true);
        winner.setColor(Color.BLACK);
        winner.setFontScale(1.2f);
        scrollTable.add(winner).width(300);
        scrollTable.row();

        Table group = new Table();
        group.add(win_circle).width(100).height(100).padRight(10);
        group.add(win_cross).width(100).height(100).padLeft(10);
        scrollTable.add(group).align(Align.center);
        scrollTable.row();

        Label winner_description = new Label("Falls ein Spieler gewonnen hat, dann aendert sich die Farbe der Gewinnerreihe in die Farbe Grün. "+
                                                    "Dies dient dazu, um schnell zu erkennen, wo die Gewinn-Felder zu sehen sind.", skin);
        winner_description.setAlignment(Align.left);
        winner_description.setWrap(true);
        winner_description.setColor(Color.ORANGE);
        scrollTable.add(winner_description).width(340);

        scrollBox.add(scrollPane).width(410).height(432);
        scrollBox.setPosition(TTTGame.WIDTH/4 - 315, TTTGame.HEIGHT/4 - 105);
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(scroll_background);
        stage.addActor(scrollBox);
        stage.addActor(back_button);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        scroll_background.clear();
        cross.clear();
        circle.clear();
        stage.dispose();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width, float heightmodifier) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
        button.setPosition(TTTGame.WIDTH/2-button.getWidth()/2, TTTGame.HEIGHT/2+heightmodifier);
    }
}
