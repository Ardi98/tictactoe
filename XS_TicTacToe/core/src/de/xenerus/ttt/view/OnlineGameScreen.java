package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.TileStateEnum;
import de.xenerus.ttt.model.TTTPlayer;

import java.util.HashMap;

public class OnlineGameScreen implements Screen {

    private TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private Texture background;

    private HashMap<Image, TileStateEnum> tile_map;

    public OnlineGameScreen(TTTGame game, TTTPlayer player) {
        this.game = game;
        skin = TTTGame.getSkin();
        tile_map = new HashMap<Image, TileStateEnum>();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
