package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;

public class AboutScreen implements Screen {

    TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private Texture background;

    private TextButton back_button;

    private Label dev;
    private Label email;
    private Label license;
    private Label license_title;

    private Table about_table;

    private Image about_background;

    public AboutScreen(TTTGame game) {
        this.game = game;
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        about_background = new Image(new Texture("ttt_files/TTT_ABOUT_BACKGROUND.png"));
        about_background.setSize(1200, 400);
        about_background.setPosition(TTTGame.WIDTH/2 - about_background.getWidth()/2, TTTGame.HEIGHT/2 - about_background.getHeight()/2 + 70);

        back_button = new TextButton("Umkehren zum Menu", skin, "small");
        createButton(back_button, Color.RED, Color.ORANGE, 200, -300);
        back_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
        });

        BitmapFont font;
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sanford.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        parameter.minFilter = Texture.TextureFilter.Nearest;
        parameter.genMipMaps = true;
        parameter.size = 26;
        font = generator.generateFont(parameter);

        about_table = new Table();
        about_table.setPosition(TTTGame.WIDTH/2 - 300, TTTGame.HEIGHT/2);

        dev = new Label("Entwickler: Ardi Pelaj", new Label.LabelStyle(font, Color.RED));
        email = new Label("E-Mail: ardi98@live.de", new Label.LabelStyle(font, Color.RED));
        license_title = new Label("Lizenz:", new Label.LabelStyle(font, Color.RED));
        license = new Label("Diese Software darf nur für den privaten Gebrauch benutzt werden."+
                                "\nVervielfältigung dieser Software ist in jeder Hinsicht verboten!"+
                                "\nDie Weitergabe dieser Software ist nicht gestattet!"+
                                "\nDer Eigenverkauf dieser Software ist nicht erlaubt!"+
                                "\nDie kommerzielle Nutzung dieser Software wird ohne Erlaubnis des Entwicklers\nnicht gestattet!", new Label.LabelStyle(font, Color.RED));

        dev.setPosition(TTTGame.WIDTH/2 - dev.getWidth()/2, TTTGame.HEIGHT/2 + 200);
        email.setPosition(TTTGame.WIDTH/2 - email.getWidth()/2, TTTGame.HEIGHT/2 + 150);
        license_title.setPosition(TTTGame.WIDTH/2 - license_title.getWidth()/2, TTTGame.HEIGHT/2 + 100);
        license.setPosition(TTTGame.WIDTH/2 - license.getWidth()/2, TTTGame.HEIGHT/2 - license.getHeight()/2);
        license.setAlignment(Align.center);

    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(about_background);
        stage.addActor(back_button);
        stage.addActor(dev);
        stage.addActor(email);
        stage.addActor(license_title);
        stage.addActor(license);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width, float heightmodifier) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
        button.setPosition(TTTGame.WIDTH/2-button.getWidth()/2, TTTGame.HEIGHT/2+heightmodifier);
    }
}
