package de.xenerus.ttt.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.xenerus.ttt.controller.TTTGame;
import de.xenerus.ttt.data.MusicEnum;
import de.xenerus.ttt.model.TTTAnimator;

/**
 * @author: Ardi Pelaj
 * 
*/
public class MainMenuScreen implements Screen {

    private TTTGame game;

    private OrthographicCamera camera;
    private StretchViewport viewport;
    private Stage stage;
    private Skin skin;

    private Texture background;
    private Image title;
    private Image frame;

    private TextButton start_button;
    private TextButton help_button;
    private TextButton settings_button;
    private TextButton quit_button;

    private TextureAtlas atlas;
    private Array<TextureAtlas.AtlasRegion> turningFrames;
    private static float FRAME_DUR = 0.05f;
    private Animation<?> turningAnimation;
    private TTTAnimator animator;

    private final Actor actor;

    private Label version;
    private Label hover_version;

    public MainMenuScreen(TTTGame game) {
        this.game = game;
        TTTGame.getMusicManager().playSound(MusicEnum.GAME_MUSIC);
        skin = TTTGame.getSkin();
        background = new Texture("ttt_files/TTT_BACKGROUND2.png");
        title = new Image(new Texture("ttt_files/TTT_TITLE.png"));
        title.setPosition(TTTGame.WIDTH/2-title.getWidth()/2, TTTGame.HEIGHT/2+200);
        frame = new Image(new Texture("ttt_files/TTT_FRAME.png"));
        frame.setPosition(TTTGame.WIDTH/2 + 348, TTTGame.HEIGHT/2 - 292);
        frame.setColor(new Color(Color.rgba8888(0.32f,0.0f,0.0f,1)));

        hover_version = new Label("First released Version of TTTGame!", skin);
        hover_version.setColor(Color.BLACK);
        hover_version.setVisible(false);

        version = new Label(TTTGame.VERSION, skin);
        version.setColor(Color.RED);
        version.setPosition(TTTGame.WIDTH/2 - version.getWidth()/2 - 580, TTTGame.HEIGHT/2 - version.getHeight()/2 - 330);
        version.addListener(new InputListener() {
            @Override
            public boolean mouseMoved(InputEvent event, float x, float y) {
                hover_version.setPosition(x + 20, y + 20);
                if(!hover_version.isVisible()) {
                    hover_version.setVisible(true);
                }
                return false;
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if(!hover_version.isVisible()) {
                    hover_version.setVisible(true);
                }
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if(hover_version.isVisible()) {
                    hover_version.setVisible(false);
                }
            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(!hover_version.isVisible()) {
                    hover_version.setVisible(true);
                }
                return false;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if(!hover_version.isVisible()) {
                    hover_version.setVisible(true);
                }
            }
        });

        //START BUTTON
        start_button = new TextButton("Starte Spiel", skin, "small");
        createButton(start_button, Color.RED, Color.ORANGE, 200, 100);
        start_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new StartScreen(game));
                return true;
            }
        });

        //HELP BUTTON
        help_button = new TextButton("Anleitung", skin, "small");
        createButton(help_button, Color.RED, Color.ORANGE, 200, 0);
        help_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new HelpScreen(game));
                return true;
            }
        });

        //SETTINGS BUTTON
        settings_button = new TextButton("Einstellungen", skin, "small");
        createButton(settings_button, Color.RED, Color.ORANGE, 200, -100);
        settings_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new SettingsScreen(game));
                return true;
            }
        });

        //QUIT BUTTON
        quit_button = new TextButton("Beenden", skin, "small");
        createButton(quit_button, Color.RED, Color.ORANGE, 200, -200);
        quit_button.addListener(new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                System.out.println("Spiel beendet!");
                dispose();
                return true;
            }
        });

        atlas = new TextureAtlas(Gdx.files.internal("animation/question/charset.atlas"));
        turningFrames = atlas.findRegions("drehen");
        turningAnimation = new Animation<>(FRAME_DUR, turningFrames, Animation.PlayMode.LOOP);
        animator = new TTTAnimator(turningAnimation);
        animator.setPosition(TTTGame.WIDTH/2 + 380, TTTGame.HEIGHT/2 - 260);

        actor = new Actor();
        actor.setSize(40, 62);
        actor.setPosition(TTTGame.WIDTH/2 + 390, TTTGame.HEIGHT/2 - 260);
        actor.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TTTGame.getMusicManager().playSound(MusicEnum.SOUND_BUTTON);
                game.setScreen(new AboutScreen(game));
            }
        });
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(TTTGame.WIDTH, TTTGame.HEIGHT);
        viewport = new StretchViewport(TTTGame.WIDTH, TTTGame.HEIGHT, camera);
        stage = new Stage(viewport);

        stage.addActor(title);
        stage.addActor(version);
        stage.addActor(hover_version);
        stage.addActor(frame);
        stage.addActor(actor);
        stage.addActor(start_button);
        stage.addActor(help_button);
        stage.addActor(settings_button);
        stage.addActor(quit_button);
        stage.addActor(animator);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.102f, 0.102f, 0.102f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        TTTGame.spriteBatch.begin();
        TTTGame.spriteBatch.setTransformMatrix(camera.view);
        TTTGame.spriteBatch.setProjectionMatrix(camera.projection);
        TTTGame.spriteBatch.draw(background, 0, 0, TTTGame.WIDTH, TTTGame.HEIGHT);
        TTTGame.spriteBatch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Gdx.app.exit();
    }

    public void createButton(TextButton button, Color color_btn, Color color_label, float btn_width, float heightmodifier) {
        button.setTransform(true);
        button.setWidth(btn_width);
        button.setColor(color_btn);
        button.getLabel().setColor(color_label);
        button.setPosition(TTTGame.WIDTH/2-button.getWidth()/2, TTTGame.HEIGHT/2+heightmodifier);
    }
}
